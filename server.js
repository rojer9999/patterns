const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bodyParser = require('body-parser');
const users = require('./users.json');
const txtgen = require('txtgen');
const { Factory } = require('./factory');

require('./passport.config');

server.listen(3000);

app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(bodyParser.json());

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/race', function (req, res) {
  res.sendFile(path.join(__dirname, 'race.html'));
});

app.get('/login', function (req, res) {
  res.sendFile(path.join(__dirname, 'login.html'));
});

app.post('/login', function (req, res) {
  const userFromReq = req.body;
  const userInDB = users.find(user => user.login === userFromReq.login);
  if (userInDB && userInDB.password === userFromReq.password) {
    const token = jwt.sign(userFromReq, 'someSecret');
    res.status(200).json({ auth: true, token });
  } else {
    res.status(401).json({ auth: false });
  }
});

let text = '';
app.get('/text', passport.authenticate('jwt', { session:false }), function (req, res) {
  res.send({ text });
});

let isRace = false;
let isTimeCounting = false;
const racers = {};
const raceCars = ['вишнева Дев\'ятка', 'зелена Копійка', 'жовтий Богдан', 'червоний комбайн'];
let startedAt;
let innerTimer;

io.on('connection', socket => {
  console.log(`A new user connected to ${'raceRoom'}`);
  
  // FACTORY
  const factory = new Factory();

  // HIGHER-ORDER FUNCTION
  const withTimer = (func) => {
    setTimeout(func, 1500);
  }

  const setRandomCar = (raceCars) => {
    const randomCarIndex = Math.floor(Math.random() * raceCars.length);
    return raceCars[randomCarIndex];
  }

  const newUserHandler = ({ token }) => {
    if (jwt.verify(token, 'someSecret')) {
      const { login, iat } = jwt.decode(token);
      racers[iat] = {
        id: iat,
        login,
        enteredSymbols: 0,
        socketId: socket.id,
        raceCar: setRandomCar(raceCars),
        ratio: 0
      };
      io.sockets.emit('showRacers', { racers });
    }
  }

  // PROXY
  const newUserHandlerProxy = new Proxy(newUserHandler, {
    apply: (target, thisArgs, args) => {
      const newUser = factory.create('newUser');
      io.sockets.emit('comment', { comment: newUser.getComment() });
      return target.apply(thisArgs, args);
    }
  });
  
  socket.on('newUser', newUserHandlerProxy);
  
  const onRaceFinished = (timerId, racers) => {
    io.in('raceRoom').emit('raceFinished', { racers });
    clearTimeout(timerId);
  }

  // PROXY
  const onRaceEndProxy = new Proxy(onRaceFinished, {
    apply: (target, thisArg, args) => {
      const raceFinish = factory.create('raceFinish', ({ racers: args[1] }));
      io.sockets.emit('comment', { comment: raceFinish.getComment() });
      return target.apply(thisArg, args);
    }
  });

  if (!isRace) {
    socket.join('raceRoom');
    
    if (!isTimeCounting) {
      text = txtgen.paragraph();
      let seconds = 10; // PREPARING FOR START
      isTimeCounting = true;
      const tick = () => {
        timerId = setTimeout(tick, 1000);
        if (!seconds) {
          const racersList = factory.create('racersList', { racers });
          io.sockets.emit('comment', { comment: racersList.getComment() });
          
          isTimeCounting = false;
          isRace = true;
          clearTimeout(timerId);
          startedAt = Date.now();
          
          let sec = 120; // RACE TIME
          const innerTick = () => {
            innerTimerId = setTimeout(innerTick, 1000);
            innerTimer = innerTimerId;
            if (sec >= 0) {
              if (sec % 30 === 0) {
                const racersArr = Object.values(racers);
                const every30Sec = factory.create('every30Sec', { racers: racersArr });
                io.sockets.emit('comment', { comment: every30Sec.getComment() });
              }
              io.sockets.emit('finishIn', { time: sec-- });
            } else {
              const raceTime = Date.now() - startedAt;
              for (const key in racers) {
                if (racers.hasOwnProperty(key)) {
                  const racer = racers[key];
                  racer.raceTime = racer.raceTime || raceTime;
                  racer.id = key;
                  racer.ratio = (racer.enteredSymbols / raceTime) * 10 ** 20;
                  io.in('raceRoom').emit('win', { racer });
                }
              }
              withTimer(() => onRaceEndProxy(innerTimerId, racers));
            }
          }
          let innerTimerId = setTimeout(innerTick, 1000);
        }
        io.sockets.emit('timeLeft', { time: seconds-- });
      };
      let timerId = setTimeout(tick, 1000);
    }
  } else {
    socket.emit('wait', { message: 'Just wait for the next race.' });
  }

  const enteredSymbolsHandler = ({ token, enteredSymbols, textLength }) => {
    if (jwt.verify(token, 'someSecret')) {
      const { iat } = jwt.decode(token);
      const racer = racers[iat];
      racer.id = iat;
      racer.enteredSymbols = enteredSymbols;
      racer.textLength = textLength;

      io.in('raceRoom').emit('typing', { racer });

      if (enteredSymbols === textLength) {
        racer.raceTime = Date.now() - startedAt;
        racer.ratio = enteredSymbols / racer.raceTime;
        io.in('raceRoom').emit('win', { racer });

        let winners = 0;
        for (const key in racers) {
          if (racers.hasOwnProperty(key)) {
            const { enteredSymbols, textLength } = racers[key];
            if (enteredSymbols === textLength) {
              winners += 1;
            }
          }
        }

        const finish = factory.create('finish', { racer });
        io.sockets.emit('comment', { comment: finish.getComment() });
        
        if (winners === Object.keys(racers).length) {
          withTimer(() => onRaceEndProxy(innerTimer, racers));
        }
      }
    }
  };

  // PROXY
  const enteredSymbolsHandlerProxy = new Proxy(enteredSymbolsHandler, {
    apply: (target, thisArg, args) => {
      if (args[0].textLength - args[0].enteredSymbols === 30) {
        const racersArr = Object.values(racers);
        const thirtySymbolsLeft = factory.create('thirtySymbolsLeft', { racers: racersArr });
        io.sockets.emit('comment', { comment: thirtySymbolsLeft.getComment() });
      }
      return target.apply(thisArg, args);
    }
  });
  
  socket.on('enteredSymbols', enteredSymbolsHandlerProxy);

  socket.on('joinTheRace', () => socket.join('raceRoom'));

  socket.on('disconnect', () => {
    socket.leave('raceRoom');
    io.sockets.emit('disconnected', { racers, socketId: socket.id });

    if (io.engine.clientsCount === 0) {
      isRace = false;
    }
  })
});
