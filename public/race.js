const jwt = localStorage.getItem('jwt');
const uri = 'http://localhost:3000';

const keyupListener = (ev, text, socket) => {
  const value = !ev ? '' : ev.target.value;
  const enteredSymbols = value.length;
  const textPart = text.slice(0, enteredSymbols);
  const span = document.createElement('span');
  const textEl = document.querySelector('.text p');

  const redSpan = document.createElement('span');
  redSpan.setAttribute('style', 'color:red; border-bottom:2px solid red; font-weight: bold;');
  redSpan.innerText = text.slice(enteredSymbols, 1);
  textEl.insertBefore(redSpan, textEl.firstChild);

  if (value === textPart && value !== text) {
    span.innerText = value;

    const nextSymbol = text[enteredSymbols];
    redSpan.innerText = nextSymbol;
    span.appendChild(redSpan, textEl.firstChild);

    const restText = text.slice(enteredSymbols + 1);
    textEl.innerText = restText;
    textEl.insertBefore(span, textEl.firstChild);
    
    if (ev) {
      socket.emit('enteredSymbols', { 
        token: jwt,
        enteredSymbols,
        textLength: text.length
       });
    }
  }

  if (value === text) {
    const textarea = document.getElementById('textarea');
    textarea.setAttribute('disabled', true);
    
    if (ev) {
      socket.emit('enteredSymbols', { 
        token: jwt,
        enteredSymbols,
        textLength: text.length
       });
    }
  }
}

const createDiv = (myClass = '') => {
  const div = document.createElement('div');
  div.className = myClass;
  return div;
}

const race = (text, socket) => {
  const textDiv = createDiv('text');

  const p = document.createElement('p');
  const textarea = document.createElement('textarea');
  textarea.id = 'textarea';
  textarea.setAttribute('disabled', true);

  textDiv.appendChild(p);
  textDiv.appendChild(textarea);
  
  const root = document.querySelector('.race');
  root.appendChild(textDiv);
  
  keyupListener(null, text);
  textarea.addEventListener('keyup', ev => keyupListener(ev, text, socket));
}
  
const renderTimer = (sec) => {
  const time = document.getElementById('time');
  let seconds = sec;
    time.innerText = seconds;
    if (!seconds) {
      time.innerText = '';
      const textarea = document.getElementById('textarea');
      textarea.disabled = false;
      textarea.focus();
    }
    seconds--;
}

if (!jwt) {
  location.replace('/login');
} else {
  const socket = io.connect(uri);
  
  socket.emit('newUser', { token: jwt });
  socket.emit('joinTheRace');

  let receivedText;

  // COUNTDOWN TO START
  socket.on('timeLeft', ({ time }) => {
    renderTimer(time);

    if (!receivedText) {
      fetch(`${uri}/text`, {
        method: 'GET',
        withCredentials: true,
        credentials: 'include',
        headers: {
            'Authorization': `Bearer ${jwt}`,
            'Content-Type': 'application/json'
        }})
          .then(text => text.json())
          .then(({ text }) => {
            race(text, socket);
            receivedText = text;
          })
          .catch(err => console.error(err));
    }
  });
      
  // PREPARING FOR THE RACE
  socket.on('wait', ({ message }) => {
    const root = document.querySelector('.race');
    root.innerHTML = '';
    root.innerText = message;
  })

  // CREATING PROGRESSBARS
  const users = createDiv('users');
  socket.on('showRacers', ({ racers }) => {
    users.innerText = '';
    
    for (const key in racers) {
      if (racers.hasOwnProperty(key)) {
        const progress = createDiv('progress');
        const bar = createDiv('bar');
        progress.id = key;
        const login = document.createElement('p');
        login.classList = 'login';
        login.innerHTML = racers[key].login;
        
        progress.appendChild(login);
        progress.appendChild(bar);
        users.appendChild(progress);
      }
    }
  });
  const root = document.querySelector('.race');
  root.appendChild(users);

  // TYPING
  socket.on('typing', ({ racer: { id, enteredSymbols, textLength } }) => {
    const progressbars = document.querySelectorAll('.progress');
    progressbars.forEach((progressbar) => {
      if (progressbar.id == id) {
        progressbar.childNodes.forEach(el => {
          if (el.classList.contains('bar')) {
            el.style.width = enteredSymbols / textLength * 100 + '%';
          }
        })
      }
    })
  });

  // FINISH
  socket.on('win', ({ racer }) => {
    const progressbars = document.querySelectorAll('.progress');
    const { id, login, raceTime } = racer;
    progressbars.forEach((progressbar) => {
      if (progressbar.id == id) {
        progressbar.childNodes.forEach(el => {
          if (el.classList.contains('login')) {
            el.innerText = `FINISH!`;
          }
        })
      }
    })
  });

  // COUNTDOWN TO FINISH
  const countdown = document.createElement('div');
  countdown.id = 'countdown';
  root.appendChild(countdown);
  
  socket.on('finishIn', ({ time }) => {
    countdown.innerText = time || '';
    if (!time) {
      const textarea = document.getElementById('textarea');
      textarea.setAttribute('disabled', true);
    }
  });

  // WINNERS
  socket.on('raceFinished', ({ racers }) => {
    const root = document.querySelector('.race');
    root.innerHTML = '';
    const racersArr = [];
    for (const key in racers) {
      if (racers.hasOwnProperty(key)) {
        racersArr.push(racers[key]);
      }
    }
    racersArr.sort((a, b) => (a.ratio < b.ratio) ? 1 : ((b.ratio < a.ratio) ? -1 : 0));
    racersArr.forEach((racer, i) => {
      const winner = createDiv('winner');
      winner.innerText = `${++i} ${racer.login}`;
      root.appendChild(winner);
    })
  });

  // ON DISCONNECT
  socket.on('disconnected', ({ racers, socketId }) => {
    const progressbars = document.querySelectorAll('.progress');
    
    for (const key in racers) {
      if (racers.hasOwnProperty(key)) {
        const element = racers[key];
        if (element.socketId == socketId) {
          progressbars.forEach((progressbar) => {
            if (progressbar.id == element.id) {
              progressbar.childNodes.forEach(el => {
                if (el.classList.contains('login')) {
                  el.innerText = `${element.login} was disconnected`;
                }
              })
            }
          })
        }
      }
    }
  })

  // COMMENT
  socket.on('comment', (payload) => {
    const comment = document.getElementById('comment');
    comment.innerText = payload.comment;
  })
}