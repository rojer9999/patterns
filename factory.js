class NewUser {
  getComment() {
    return 'Пані та панове, вітаю всіх на цьому святі адреналіну! Сьогодні у нас в селі велика подія, яка коментує ваш улюблений Семен Михайлович!';
  }
}

class RacersList {
  constructor ({ racers }) {
    this.racers = racers;
  }

  getComment() {
    const getIntroducing = () => {
      let str = '';
      for (const key in this.racers) {
        if (this.racers.hasOwnProperty(key)) {
          const { login, raceCar } = this.racers[key];
          str += `${login} та його ${raceCar}, `;
        }
      }
      return str.slice(0, str.length - 2) + '.';
    }
    
    return `Отже, гонка почалася! Сьогодні в ній беруть участь: ${getIntroducing()}`;
  }
}

class Every30Sec {
  constructor ({ racers }) {
    this.racers = racers;
  }

  getComment() {
    const restStr = Factory.getIntroducing(this.racers, Factory.whoAndWhere);
    return `... гонка все ще триваэ і результати у нас такі: ${restStr}`;
  }
}

class ThirtySymbolsLeft {
  constructor ({ racers }) {
    this.racers = racers;
  }

  getComment() {
    const restStr = Factory.getIntroducing(this.racers, Factory.whoAndWhere);
    return `... фініш вже так близько! ${restStr}`;
  }
}

class Finish {
  constructor ({ racer }) {
    this.login = racer.login;
  }

  getComment() {
    return `... і фінішну пряму перетинає ${this.login}!`;
  }
}

class RaceFinish {
  constructor ({ racers }) {
    this.racers = racers;
  }

  getComment() {
    const getStr = (racersArr) => {
      let str = '';
      racersArr.forEach(({ login, raceTime }, i) => {
        if (i < 3) {
          const seconds = new Date(raceTime).getSeconds();
          str += `${++i} місце займає ${login}, витративши на заїзд ${seconds} секунд, `;
        }
      });
      return str;
    }

    return `... та ${Factory.getIntroducing(this.racers, getStr)}. Від імені федерації клавогонок України вітаю переможців і нагадую про дискотеку в будинку культури на честь переможця!`;
  }
}

// FACTORY
class Factory {
  create (eventType, data = {}) {
    switch (eventType) {
      case 'newUser':
        return new NewUser();
        
      case 'racersList':
        return new RacersList(data);

      case 'every30Sec':
        return new Every30Sec(data);

      case 'thirtySymbolsLeft':
        return new ThirtySymbolsLeft(data);

      case 'finish':
        return new Finish(data);

      case 'raceFinish':
        return new RaceFinish(data);
    }
  }

  static getIntroducing (racers, getStr) {
    const racersArr = [];
    for (const key in racers) {
      if (racers.hasOwnProperty(key)) {
        racersArr.push(racers[key]);
      }
    }
    racersArr.sort((a, b) => (a.ratio < b.ratio) ? 1 : ((b.ratio < a.ratio) ? -1 : 0));

    const formatedStr = getStr(racersArr).slice(0, getStr(racersArr).length - 2);
    return `${formatedStr}`;
  }

  static whoAndWhere (racersArr) {
    let str = '';
    racersArr.forEach(({ login, raceCar }, i) => {
      if (i === 0) {
        str += `Першим йде ${login}, і його ${raceCar} ,`;
      } else {
        str += `за ним ${login}, `
      }
    });
    return str;
  }
}

module.exports = {
  Factory
}